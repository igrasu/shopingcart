$(document).ready(function() {

	var products = [{
			"index": 0,
			"name": "Huawei P20 Lite(2017)",
			"brand": "Huawei",
			"price": 10,
			"image": "./images/img_1.jpg",
			"memory": 6,
			"weight": "128g",
			"sim": "Nano SIM",
			"camera": 12,
			"rating": 4,
			"color": ["black", "red"]
		}, {
			"index": 1,
			"name": "Samsung Galaxy A8 (2018)",
			"brand": "Samsung",
			"price": 20,
			"image": "./images/img_2.jpg",
			"memory": 16,
			"weight": "214g",
			"sim": "Micro SIM",
			"camera": 15,
			"rating": 3
		}, {
			"index": 2,
			"name": "Apple iPhone X",
			"brand": "Apple",
			"price": 30,
			"image": "./images/img_3.jpg",
			"memory": 12,
			"weight": "428g",
			"sim": "Nano SIM",
			"camera": 10,
			"rating": 5
		}, {
			"index": 3,
			"name": "Samsung Galaxy J5 (2017)",
			"brand": "Samsung",
			"price": 40,
			"image": "./images/img_4.jpg",
			"memory": 5,
			"weight": "148g",
			"sim": "SIM",
			"camera": 20,
			"rating": 4
		}, {
			"index": 4,
			"name": "Huawei P40 Lite (2018)",
			"brand": "Huawei",
			"price": 50,
			"image": "./images/img_5.jpg",
			"memory": 4,
			"weight": "168g",
			"sim": "Micro SIM",
			"camera": 18,
			"rating": 1
		}, {
			"index": 5,
			"name": "Apple Iphone XS MAX",
			"brand": "Apple",
			"price": 60,
			"image": "./images/img_6.jpg",
			"memory": 3,
			"weight": "198g",
			"sim": "Nano SIM",
			"camera": 14,
			"rating": 4
		}, {
			"index": 6,
			"name": "HTC U 11+",
			"brand": "HTC",
			"price": 70,
			"image": "./images/img_7.jpg",
			"memory": 2,
			"weight": "216g",
			"sim": "SIM",
			"camera": 11,
			"rating": 2
		}, {
			"index": 7,
			"name": "Samsung Galaxy S10",
			"brand": "Samsung",
			"price": 80,
			"image": "./images/img_8.jpg",
			"memory": 11,
			"weight": "338g",
			"sim": "Nano SIM",
			"camera": 6,
			"rating": 5
		}, {
			"index": 8,
			"name": "Allview X4 Soul Infinity Plus",
			"brand": "Allview",
			"price": 90,
			"image": "./images/img_9.png",
			"memory": 8,
			"weight": "228g",
			"sim": "Micro SIM",
			"camera": 12,
			"rating": 4
		}, {
			"index": 9,
			"name": "Samsung Galaxy S9",
			"brand": "Samsung",
			"price": 100,
			"image": "./images/img_10.jpg",
			"memory": 9,
			"weight": "128g",
			"sim": "Micro SIM",
			"camera": 15,
			"rating": 5
		}, {
			"index": 10,
			"name": "Allview X2 soul style",
			"brand": "Allview",
			"price": 110,
			"image": "./images/img_11.jpg",
			"memory": 7,
			"weight": "388g",
			"sim": "Nano SIM",
			"camera": 10,
			"rating": 3
		}, {
			"index": 11,
			"name": "Huawei Mate 20 Pro",
			"brand": "Huawei",
			"price": 120,
			"image": "./images/img_12.jpg",
			"memory": 8,
			"weight": "218g",
			"sim": "Micro SIM",
			"camera": 8,
			"rating": 1
		}
	];
	var cartProducts = [];
	var productsCompare = [];

	addPhones();

	$('.cart-btn').click(function() {
		addToArray($(this), cartProducts, false);

		var itemCount = cartProducts.length;
		$('.my-cart-badge').text(itemCount);
	});
	//cart
	$('.cart').on('click', function() {
		addProducts();
	});
	$('body').on('click', '.btn-primary', function() {
		cartProducts = [];
		var itemCount = cartProducts.length;
		$('.my-cart-badge').text(itemCount);
		addProducts();
	});
	$('body').on('click', '.removeItem', function() {
		var objIndex = $(this).parent('.cart-item').attr('data-id');
		cartProducts.splice(getByValue(cartProducts, objIndex), 1);
		var itemCount = cartProducts.length;
		$('.my-cart-badge').text(itemCount);
		addProducts();
	});

	$('.btn-compare').click(function() {
		if (productsCompare.length <= 2) {
			addToArray($(this), productsCompare, 2);

			if (productsCompare.length == 0) {
				$('.compare-list').hide();
				$('.compare-list').animate({
					opacity: 0,
				}, 500, function() {});
			}

			if (productsCompare.length == 1) {
				$('.compare-list').show();
				$('.compare-list').animate({
					opacity: 1,
				}, 500, function() {});

				addProductsToCompare();
			}

			if (productsCompare.length == 2) {
				addProductsToCompare();

				compProducts();

				$('.compare-itm').prepend('<div class="col-md-4 col-xs-4">' +
					'<div class="compare-item">' +
					'<div class="compare-img comp"><img src="./images/compare.jpg""/></div>' +
					'<div class="compare-name comp"><span>Phone Name <span></div>' +
					'<div class="compare-rating comp">Rating: </div>' +
					'<div class="compare-price comp">Price:<span></span></div>' +
					'<div class="compare-brand comp">Brand: </div>' +
					'<div class="compare-memory comp"><span>Ram Memory: <span></div>' +
					'<div class="compare-weight comp"><span>Weight: <span></div>' +
					'<div class="compare-sim comp"><span>Sim type: <span></div>' +
					'<div class="compare-camera comp"><span>Camera resolution: <span></div>' +
					'</div>' +
					'</div>');

				$('body').css('overflow', 'hidden');
				$('.mod').addClass('modal-backdrop');
				$('.compare-items').animate({
					opacity: 1,
					top: "50%"
				}, 500, function() {});
			}
		}
	});

	$('body').on('click', '.deleteItem', function() {
		var objIndex = $(this).parent('.compare-item').attr('data-id');
		productsCompare.splice(getByValue(productsCompare, objIndex), 1);

		addProductsToCompare();
		if (productsCompare.length == 1) {
			$('.compare-items').animate({
				opacity: 0,
				top: "0",
			}, 500, function() {});

			$('body').css('overflow', 'auto');
			$('.mod').removeClass('modal-backdrop');
		}
		if (productsCompare.length == 0) {
			$('.compare-list').hide(200);
			$('.compare-list').animate({
				opacity: 0,
			}, 500, function() {});
		}

	});
	$('body').on('click', '.close', function() {
		$('.compare-items').animate({
			opacity: 0,
			top: "0",
		}, 500, function() {});

		$('body').css('overflow', 'auto');
		$('.mod').removeClass('modal-backdrop');
		$('.compare-list').hide(200);
		$('.compare-list').animate({
			opacity: 0,
		}, 500, function() {});
		productsCompare = [];
	});

	function getByValue(arr, value) {
		var result = arr.filter(function(o) {
			return o.index == value;
		});
		if (result.length > 0) {
			return arr.indexOf(result[0]);
		} else {
			return null;
		}
	}

	function addToArray(thisItem, array, max) {
		var objIndex = $(thisItem).parents('.item').attr('data-id');
		var obj = products[objIndex];
		var resultIndex = getByValue(array, obj.index);

		if (resultIndex == null) {
			if (max) {
				if (array.length < max) {
					array.push(obj);
				}
			} else {
				array.push(obj);
			}
		}
	}

	function addPhones() {
		for (var i = 0; i < products.length; i++) {
			var name = products[i].name;
			var price = products[i].price;
			var image = products[i].image;
			var brand = products[i].brand;
			var rating = products[i].rating;

			var product =
				'<div class="col-md-3">' +
				'<div class="item" data-id="' + i + '" data-name="' + name.toLowerCase() + '" data-rating="' + rating + '" data-brand="' + brand.toLowerCase() + '" data-price="' + price + '">' +
				'<div class="item-info"><img src="' + image + '"/>' +
				'<div class="rating rating' + rating + '"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>' +
				'<div class="name"><span>' + name + '</span></div>' +
				'<div class="price">' + price + '<span> $ </span></div>' +
				'</div>' +
				'<div class="card-footer">' +
				'<span class="cart-btn btn">Add Cart</span>' +
				'<span class="btn-compare btn" data-toggle="modal" data-target="#myModal2">Compare</span>' +
				'</div>' +
				'</div>' +
				'</div>';

			$('.carTitems').append(product);
		}
	}

	function addProducts() {
		var priceTotal = 0;

		$('.items').empty();
		$('.cartTotal').text('');

		for (var j = 0; j < cartProducts.length; j++) {
			var name = cartProducts[j].name;
			var price = cartProducts[j].price;
			var image = cartProducts[j].image;
			var id = cartProducts[j].index;
			priceTotal += price;
			var cartProduct =
				'<li class="cart-item" data-id="' + id + '">' +
				'<div class="cart-img"><img src="' + image + '"/></div>' +
				'<div class="cart-name"><span>Product <span>' + name + '</div>' +
				'<div class="removeItem">X</div>' +
				'<div class="cart-price">' + price + '<span> $ </span></div>' +
				'</li>';
			$('.items').append(cartProduct);
		}
		$('.cartTotal').text("Total price: $" + priceTotal);
	}

	function addProductsToCompare() {
		$('.compare').empty();
		for (var x = 0; x < productsCompare.length; x++) {
			var name = productsCompare[x].name;
			var image = productsCompare[x].image;
			var id = productsCompare[x].index;
			var prodCompareItem =
				'<li class="compare-item " data-id="' + id + '">' +
				'<div class="compare-img"><img src="' + image + '"/></div>' +
				'<div class="compare-name"><span>Product <span>' + name + '</div>' +
				'<div class="deleteItem">X</div>' +
				'</li>';
			$('.compare').append(prodCompareItem);
		}
	}

	function compProducts() {
		$('.compare-itm').empty();
		for (var x = 0; x < productsCompare.length; x++) {
			var name = productsCompare[x].name,
				price = productsCompare[x].price;
			var image = productsCompare[x].image;
			var memory = productsCompare[x].memory;
			var weight = productsCompare[x].weight;
			var sim = productsCompare[x].sim;
			var camera = productsCompare[x].camera;
			var brand = productsCompare[x].brand;
			var rating = productsCompare[x].rating;
			var prodCompareItem =
				'<div class="col-md-4 col-xs-4">' +
				'<div class="compare-item">' +
				'<div class="compare-img comp"><img src="' + image + '"/></div>' +
				'<div class="compare-name comp">' + name + '</div>' +
				'<div class="rating comp rating' + rating + '"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>' +
				'<div class="compare-price comp">' + price + '<span> $ </span></div>' +
				'<div class="compare-brand comp">' + brand + '</div>' +
				'<div class="compare-memory comp">' + memory + '<span> GB </span></div>' +
				'<div class="compare-weight comp">' + weight + '</div>' +
				'<div class="compare-sim comp">' + sim + '</div>' +
				'<div class="compare-camera comp">' + camera + '<span> Mp </span></div>' +
				'</div>' +
				'</div>';

			$('.compare-itm').append(prodCompareItem);

		}
	}

	var filter = {
		brands: [],
		price: [null, null],
		name: '',
		rating: [],
	};

	function filterByType() {
		$(".item").each(function() {
			filterBrand = $(this).data('brand').toString();
			filterRating = $(this).data('rating').toString();
			filterPrice = $(this).data('price');
			filterName = $(this).data('name').toString();
			
			var filterByBrand = true;
			if (filter.brands.length !== 0) {
				filterByBrand = filter.brands.indexOf(filterBrand) > -1;
			}
			var filterByRating = true;
			if (filter.rating.length !== 0) {
				filterByRating = filter.rating.indexOf(filterRating) > -1;
			}
			var filterByPrice = true;
			if (filter.price[0] !== null && filter.price[1] !== null) {
				filterByPrice = (filterPrice >= filter.price[0] && filterPrice <= filter.price[1]) ||
					(isNaN(filter.price[0]) && filterPrice <= filter.price[1]) ||
					(filterPrice >= filter.price[0] && isNaN(filter.price[1]));
			}
			var filterByName = true;
			if (filter.name.length !== 0) {
				filterByName = filterName.indexOf(filter.name) > -1;
			}

			if (filterByBrand && filterByRating && filterByName && filterByPrice) {
				$(this).parent().show();
			} else {
				$(this).parent().hide();
			}
		});
	}

	$(".filter-star input").on('click', function() {
		var thisType = $(this).val().toLowerCase();
		if ($(this).is(':checked')) {
			filter.rating = thisType;
		}
		filterByType();
	});
	$(".filter-brand input").on('click', function() {
		var thisType = $(this).val().toLowerCase();
		if ($(this).is(':checked')) {
			filter.brands.push(thisType);
		} else {
			filter.brands.splice(filter.brands.indexOf(thisType), 1);
		}

		filterByType();
	});


	$('.search-bar').on('keyup', function() {
		var searchInput = $(this).val().toLowerCase();
		filter.name = searchInput;
		filterByType();
	});

	$('#filter').click(function() {
		var budgetMin = parseInt($('#min').val()),
			budgetMax = parseInt($('#max').val());

		filter.price[0] = budgetMin;
		filter.price[1] = budgetMax;
		console.log(filter.price[0]);

		filterByType();
	});
	$('#reset').click(function() {
		$('#min').val(0);
		$('#max').val(120);

		var budgetMin = parseInt($('#min').val()),
			budgetMax = parseInt($('#max').val());

		filter.price[0] = budgetMin;
		filter.price[1] = budgetMax;
		filterByType();

	});
	$(".btn-filter").on('click', function() {
		$(".filter").slideToggle(500);
	});
	$(window).scroll(function() {
		if ($(this).scrollTop() > 140) {
			$('.header').addClass('fixed');
		} else {
			$('.header').removeClass('fixed');
		}
	});

});